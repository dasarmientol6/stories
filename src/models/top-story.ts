import { Schema, model } from 'mongoose';

/**
 * Clase de la story
 *
 * @author Diego Sarmiento - Nov, 06-2021
 */
const TopStorySchema = new Schema({
    by: String,
    descendants: Number,
    kids:[],
    score: Number,
    time: Number,
    title : String,
    type : String,
    url: String
});

export default model('Task', TopStorySchema);