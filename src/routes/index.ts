import { Router, Request, Response } from 'express';
import axios, { AxiosResponse } from 'axios';

const router = Router();

/**
 * Metodo que obtiene las stories de la api
 *
 * @author Diego Sarmiento - Nov, 06-2021
 */
router.get('/', async (req: Request, res: Response) => {
    try {
        const result: AxiosResponse = await axios.get('https://hacker-news.firebaseio.com/v0/topstories.json');

        let dataStories = [];

        for (let index = 0; index < 16; index++) {
            const resultById: AxiosResponse = await axios.get('https://hacker-news.firebaseio.com/v0/item/' + result.data[index] + '.json');
            dataStories.push(resultById.data);
        }

        res.render('index', { dataStories });
    } catch (err) {
        console.error('error', err)
    }
});

/**
 * Metodo que obtiene las stories de la api
 *
 * @author Diego Sarmiento - Nov, 06-2021
 */
router.route('/search')
    .post(async (req: Request, res: Response) => {
        const result: AxiosResponse = await axios.get('https://hacker-news.firebaseio.com/v0/topstories.json');
        
        // Posiciones en las que se quiere ver la informacion
        const i = req.body.i;
        const n = req.body.n;
        let dataStories = [];
        
        // Se valida si los valores solicitados son menores al valor total de los resultados
        // para no provocar un danio en la app
        if (i >= result.data.length || n >= result.data.length) {
            res.render('index', { dataStories });
            return;
        }

        // Se obtienen los resultados de las stories
        for (let index = n; index <= i; index++) {
            const resultById: AxiosResponse = await axios.get('https://hacker-news.firebaseio.com/v0/item/' + result.data[index] + '.json');
            dataStories.push(resultById.data);
        }

        // Se retornan los resultados a la vista
        res.render('index', { dataStories });
    });

export default router;